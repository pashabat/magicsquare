package magic_square;


public class Main {

    public static void main(String[] args) {
        MagicSquare square = new MagicSquare();
        square.generateSquare();
        SaveSquare.save(square, "src\\main\\resources\\MagicSquare.txt");
    }

}
