package magic_square;

import java.io.FileWriter;
import java.io.IOException;


public class SaveSquare {

    private static final String TOP_LINE_INDENT = "     ";
    private static final String TOP_LINE_SEPARATOR = " | ";
    private static final String LINE_SEPARATOR = "   ";
    private static final String ADDITIONAL_LINE_SEPARATOR = " ";    // if number < 10


    public static void save(MagicSquare square, String fileTitle) {
        try (FileWriter writer = new FileWriter(fileTitle, false)) {
            writeTopLine(writer, square.getColumns());
            writer.append(System.lineSeparator());

            for (int row = 0; row < square.getRows().length; row++) {
                writeNumber(writer, square.getRows()[row]);

                for (int column = 0; column < square.getColumns().length; column++) {
                    writeNumber(writer, square.getSquare()[row][column]);
                }
                writer.append(System.lineSeparator());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void writeTopLine(FileWriter writer, int[] columns) throws IOException {
        writer.write(TOP_LINE_INDENT);
        for (int column = 0; column < columns.length; column++) {
            if (column > 0) {
                writer.write(TOP_LINE_SEPARATOR);
            }
            writer.write(String.valueOf(columns[column]));
            if (columns[column] < 10) {
                writer.write(ADDITIONAL_LINE_SEPARATOR);
            }
        }
    }

    private static void writeNumber(FileWriter writer, int number) throws IOException {
        writer.write(String.valueOf(number));
        writer.write(LINE_SEPARATOR);
        if (number < 10) {
            writer.write(ADDITIONAL_LINE_SEPARATOR);
        }
    }
}
