package magic_square;

import java.util.Random;


public class MagicSquare {

    private int squareSize;
    private int maxValue;
    private int[][] square;
    private int[] rows;
    private int[] columns;


    public MagicSquare() {
        squareSize = 10;
        maxValue = 100;
    }

    public MagicSquare(int squareSize, int maxValue) {
        this.squareSize = squareSize;
        this.maxValue = maxValue;
    }

    public void generateSquare() {
        square = new int[squareSize][squareSize];

        rows = generateSideNumbers();
        do {
            columns = generateSideNumbers();
        } while (checkSquareSides());

        fillSquare();
    }

    private boolean checkSquareSides() {
        return sideSum(rows) != sideSum(columns);
    }

    private int sideSum(int[] side) {
        int sum = 0;
        for (int number : side) {
            sum += number;
        }
        return sum;
    }

    private void fillSquare() {
        int[] columnSum = new int[columns.length];
        int rowSum;
        for (int row = 0; row < rows.length; row++) {
            rowSum = 0;
            for (int column = 0; column < columns.length; column++) {
                int possibleMaxValue = rows[row] - rowSum;

                int possibleMaxColumnValue = columns[column] - columnSum[column];
                if (possibleMaxColumnValue < possibleMaxValue) {
                    possibleMaxValue = possibleMaxColumnValue;
                }

                square[row][column] = possibleMaxValue;

                rowSum += possibleMaxValue;
                columnSum[column] += possibleMaxValue;
            }
        }
    }

    private int[] generateSideNumbers() {
        Random random = new Random();
        int[] side = new int[squareSize];
        for (int i = 0; i < side.length; i++) {
            side[i] = random.nextInt(maxValue);
        }
        return side;
    }

    public int[][] getSquare() {
        return square;
    }

    public int[] getRows() {
        return rows;
    }

    public int[] getColumns() {
        return columns;
    }
}
