package magic_square;

import junit.framework.Assert;
import org.junit.Test;


public class MagicSquareTest {

    @Test
    public void squareTest() {
        int randomizeQualityParameter = 100;
        int matrixSize = 10;
        int maxValue = 100;

        MagicSquare square = new MagicSquare(matrixSize, maxValue);
        for (int i = 0; i < randomizeQualityParameter; i++) {
            square.generateSquare();
            Assert.assertTrue(checkSquare(square));
        }
    }

    private boolean checkSquare(MagicSquare square) {
        int columnSum;
        int[] rowSum = new int[square.getRows().length];
        for (int column = 0; column < square.getColumns().length; column++) {
            columnSum = 0;
            for (int row = 0; row < square.getRows().length; row++) {
                columnSum += square.getSquare()[row][column];
                rowSum[row] += square.getSquare()[row][column];
            }
            if (square.getColumns()[column] != columnSum) {
                return false;
            }
        }
        for (int row = 0; row < square.getRows().length; row++) {
            if (square.getRows()[row] != rowSum[row]) {
                return false;
            }
        }
        return true;
    }
}
